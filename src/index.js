import {setLocal} from "./setStorage";
import {getLocal} from "./getStorage";

let formProduct = document.querySelector('#save-product')
formProduct.addEventListener('submit',e =>{
    e.preventDefault()
    let product = document.querySelector('#product')
    let type = document.querySelector('#type')
    let obj = {
        "product": product.value,
        "type" : type.value
    }
    let localStorageProduct = getLocal('products') || [];
    localStorageProduct.push(obj)
    setLocal('products',localStorageProduct);
    alert('saved')
})
