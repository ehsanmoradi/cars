import {getLocal} from './getStorage';
import {setLocal} from './setStorage';


let formVariant = document.querySelector('#form-variant')
formVariant.addEventListener('submit',evt => {
    evt.preventDefault();
    let productSaved = document.querySelector('#product-saved')
    let color = document.querySelector('#color')
    let entity = document.querySelector('#entity')
    let obj = {
        "product_saved": productSaved.value,
        "color" : color.value,
        "entity" : entity.entity
    }
    let localStorageProduct = getLocal('variants') || [];
    localStorageProduct.push(obj)
    setLocal('variants',localStorageProduct)
    alert('saved')

})
let l = [];
let list = getLocal('products').map(v=>{
    return l.push({
        "text":v.product,
        "id":v.product
    });
})
// console.log(l);
$('#product-saved').select2({
    data: l
});
