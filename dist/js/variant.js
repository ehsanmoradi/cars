/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/var.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/getStorage.js":
/*!***************************!*\
  !*** ./src/getStorage.js ***!
  \***************************/
/*! exports provided: getLocal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getLocal\", function() { return getLocal; });\nconst getLocal = value => {\r\n    return JSON.parse(localStorage.getItem(value))\r\n}\n\n//# sourceURL=webpack:///./src/getStorage.js?");

/***/ }),

/***/ "./src/setStorage.js":
/*!***************************!*\
  !*** ./src/setStorage.js ***!
  \***************************/
/*! exports provided: setLocal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"setLocal\", function() { return setLocal; });\nconst setLocal = (localName,obj) =>{\r\n    localStorage.setItem(localName,JSON.stringify(obj))\r\n}\n\n//# sourceURL=webpack:///./src/setStorage.js?");

/***/ }),

/***/ "./src/var.js":
/*!********************!*\
  !*** ./src/var.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _getStorage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./getStorage */ \"./src/getStorage.js\");\n/* harmony import */ var _setStorage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./setStorage */ \"./src/setStorage.js\");\n\r\n\r\n\r\n\r\nlet formVariant = document.querySelector('#form-variant')\r\nformVariant.addEventListener('submit',evt => {\r\n    evt.preventDefault();\r\n    let productSaved = document.querySelector('#product-saved')\r\n    let color = document.querySelector('#color')\r\n    let entity = document.querySelector('#entity')\r\n    let obj = {\r\n        \"product_saved\": productSaved.value,\r\n        \"color\" : color.value,\r\n        \"entity\" : entity.entity\r\n    }\r\n    let localStorageProduct = Object(_getStorage__WEBPACK_IMPORTED_MODULE_0__[\"getLocal\"])('variants') || [];\r\n    localStorageProduct.push(obj)\r\n    Object(_setStorage__WEBPACK_IMPORTED_MODULE_1__[\"setLocal\"])('variants',localStorageProduct)\r\n    alert('saved')\r\n\r\n})\r\nlet l = [];\r\nlet list = Object(_getStorage__WEBPACK_IMPORTED_MODULE_0__[\"getLocal\"])('products').map(v=>{\r\n    return l.push({\r\n        \"text\":v.product,\r\n        \"id\":v.product\r\n    });\r\n})\r\n// console.log(l);\r\n$('#product-saved').select2({\r\n    data: l\r\n});\r\n\n\n//# sourceURL=webpack:///./src/var.js?");

/***/ })

/******/ });