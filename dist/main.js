/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("let formProduct = document.querySelector('#save-product')\r\nformProduct.addEventListener('submit',e =>{\r\n    e.preventDefault()\r\n    let product = document.querySelector('#product')\r\n    let type = document.querySelector('#type')\r\n    let obj = {\r\n        \"product\": product.value,\r\n        \"type\" : type.value\r\n    }\r\n    let localStorageProduct = getLocalStorage('products') || [];\r\n    localStorageProduct.push(obj)\r\n    setLocalStorage('products',localStorageProduct);\r\n    // addToSelect();\r\n})\r\n\r\nfunction getLocalStorage(value) {\r\n    return JSON.parse(localStorage.getItem(value))\r\n}\r\n\r\nfunction setLocalStorage(localName,obj) {\r\n    localStorage.setItem(localName,JSON.stringify(obj))\r\n}\r\n\r\n/*\r\nfunction addToSelect(){\r\n\r\n}\r\nwindow.addEventListener('load',ev => {\r\n    addToSelect()\r\n})\r\n*/\r\nlet l = [];\r\nlet list = getLocalStorage('products').map(v=>{\r\n    return l.push({\r\n        \"text\":v.product,\r\n        \"id\":v.product\r\n    });\r\n})\r\nconsole.log(l);\r\n$('#product-saved').select2({\r\n    data: l\r\n});\r\nlet formVariant = document.querySelector('#form-variant')\r\nformVariant.addEventListener('submit',evt => {\r\n    evt.preventDefault();\r\n    let productSaved = document.querySelector('#product-saved')\r\n    let color = document.querySelector('#color')\r\n    let entity = document.querySelector('#entity')\r\n    let obj = {\r\n        \"product_saved\": productSaved.value,\r\n        \"color\" : color.value,\r\n        \"entity\" : entity.entity\r\n    }\r\n    let localStorageProduct = getLocalStorage('variants') || [];\r\n    localStorageProduct.push(obj)\r\n    setLocalStorage('variants',localStorageProduct)\r\n\r\n})\r\n// import Chart from 'chart.js/dist/Chart.min'\r\n/*\r\nvar Chart = require('chart.js');\r\n\r\nvar ctx = document.getElementById('myChart').getContext('2d');\r\nvar myChart = new Chart(ctx, {\r\n    type: 'line',\r\n    data: {\r\n        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],\r\n        datasets: [{\r\n            label: 'My First dataset',\r\n            backgroundColor: 'rgb(255, 99, 132)',\r\n            borderColor: 'rgb(255, 99, 132)',\r\n            data: [0, 10, 5, 2, 20, 30, 45]\r\n        }]\r\n    },\r\n    options: {\r\n    }\r\n});\r\n*/\r\n/*\r\nimport ApexCharts from 'apexcharts'\r\n\r\nvar options = {\r\n    chart: {\r\n        type: 'line'\r\n    },\r\n    series: [{\r\n        name: 'sales',\r\n        data: [30,40,35,50,49,60,70,91,125]\r\n    }],\r\n    xaxis: {\r\n        categories: [1991,1992,1993,1994,1995,1996,1997, 1998,1999]\r\n    }\r\n}\r\nvar chart = new ApexCharts(document.querySelector(\"#chart\"), options);\r\n\r\nchart.render();\r\n*/\r\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });