const path = require('path')

module.exports = {
    entry : {
        main : './src/index.js',
        variant : './src/var.js',
        // chart : './src/chart.js'
    },
    output : {
        filename : '[name].js',
        path : path.resolve('E:\\project\\test\\dist','js')
    },
}
